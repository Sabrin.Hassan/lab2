package INF101.lab2;

import java.util.List;
import java.util.ArrayList; 
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    
   ArrayList<FridgeItem> foodFridge = new ArrayList<>(); 
   int sizeFood = 20; 
   

    @Override
    public int nItemsInFridge() {
        // TODO Auto-generated method stub
        int itemCount = 0;
        for(int i = 0; i < foodFridge.size(); i++) {
            FridgeItem food = foodFridge.get(i);
            if(food != null){
                itemCount += 1 ;
            }
        }
        return itemCount;
    }

    @Override
    public int totalSize() {
        // TODO Auto-generated method stub

        return sizeFood;
    }

    @Override
    public boolean placeIn(FridgeItem item){
        // TODO Auto-generated method stub
        if(sizeFood > foodFridge.size()){
            foodFridge.add(item);
            return true;
        }
        else{
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        // TODO Auto-generated method stub
        if(foodFridge.size() > 0){
            foodFridge.remove(item);
        }
        else {
            throw new NoSuchElementException();
        } 
    }

    @Override
    public void emptyFridge() {
        // TODO Auto-generated method stub
       foodFridge.removeAll(foodFridge);
    }


    @Override
    public List<FridgeItem> removeExpiredFood() {
        // TODO Auto-generated method stub
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();
        for (int i = 0; i < nItemsInFridge(); i++){
            FridgeItem item = foodFridge.get(i);
            if(item.hasExpired()){
                expiredFood.add((item));
            }

        }
        for (FridgeItem expiItem : expiredFood){
            foodFridge.remove(expiItem);
        }

        return expiredFood;
    }

    
}
